package com.sms.sendmessage;

import static com.sms.sendmessage.SampleService.NOTIF_CHANNEL_ID;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";

    private static final int PERMISSION_REQUEST_INTERNET = 1;
    private static final int PERMISSION_REQUEST_SMS = 100;
    public static final String SERVICE_INTERVAL = "serviceInterval";

    public static HandlerThread handlerThread;
    public static Handler handler;

    static EditText etPhone;
    static EditText etMessage;
    EditText etEndpoint;
    TextView txtURL;
    public TextView txtLog;
    Button btSend, btSave, btOnService;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String URL = "text";

    private static String url;
    private static DBHandler dbHandler;
    private boolean isServiceOn = false;

    public float serviceInterval = 1; // Minutes.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUIComponents();
        setupListeners();
        createNotificationChannel();

        handlerThread = new HandlerThread("BackgroundHandlerThread");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());

        dbHandler = new DBHandler(MainActivity.this);

        loadData();
        updateViewsUrl();
    }

    private void initUIComponents() {
        etPhone = findViewById(R.id.et_phone);
        etMessage = findViewById(R.id.et_message);
        etEndpoint = findViewById(R.id.et_endpoint);
        txtURL = findViewById(R.id.txt_api);
        btSend = findViewById(R.id.btSend);
        btSave = findViewById(R.id.btSave);
        btOnService = findViewById(R.id.btOnService);
        txtLog = findViewById(R.id.txt_log);
        LogHelper.setLogView(txtLog);
    }

    private void setupListeners() {
        btSend.setOnClickListener(v -> sendSMS());

        btSave.setOnClickListener(v -> {
            url = etEndpoint.getText().toString();
            txtURL.setText(url);
            saveData();
        });

        btOnService.setOnClickListener(v -> toggleService());

        View mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                hideKeyboard(v);
            }
            return false;
        });
    }

    private void sendSMS() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            String phone = etPhone.getText().toString().trim();
            String message = etMessage.getText().toString().trim();

            if (!phone.isEmpty() && !message.isEmpty()) {
                sendMessage(phone, message);
                String currentDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
                String logMessage = currentDateTime + ": " + phone;
                LogHelper.appendLog(logMessage);
            } else {
                Toast.makeText(this, "Phone or Message cannot be empty", Toast.LENGTH_SHORT).show();
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_SMS);
        }
    }

    private void toggleService() {
        isServiceOn = !isServiceOn;
        updateServiceButtonText();

        Intent serviceIntent = new Intent(this, SampleService.class);
        if (isServiceOn) {
            startService(serviceIntent);
            ContextCompat.startForegroundService(this, serviceIntent);
        } else {
            stopService(serviceIntent);
        }
    }

    private void updateServiceButtonText() {
        btOnService.setText(isServiceOn ? "Status: ON" : "Status: OFF");
        btOnService.setBackgroundResource(isServiceOn ? R.color.teal_700 : R.color.purple_200);
    }

    private void createNotificationChannel() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(NOTIF_CHANNEL_ID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        if (notificationManager != null) {
          notificationManager.createNotificationChannel(channel);
        }
      }
    }

    public static void sendMessage(String sPhone, String sMessage) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> arrSMS = smsManager.divideMessage(sMessage);
        smsManager.sendMultipartTextMessage(sPhone, null, arrSMS, null, null);
    }


    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            showSettingsDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Minutes for automatic data update");

        final EditText input = new EditText(this);
        input.setInputType(android.text.InputType.TYPE_CLASS_NUMBER | android.text.InputType.TYPE_NUMBER_FLAG_DECIMAL);
        input.setHint("Enter Minutes (0.5 = 30s)");
        input.setText(String.valueOf(serviceInterval));
        builder.setView(input);

        builder.setPositiveButton("OK", (dialog, which) -> {
            String value = input.getText().toString();
            if (!value.isEmpty()) {
                try {
                    float minutes = Float.parseFloat(value);
                    serviceInterval = minutes;
                    saveServiceInterval(minutes);
                    Toast.makeText(this, "Update interval set to: " + minutes + " minutes", Toast.LENGTH_SHORT).show();
                } catch (NumberFormatException e) {
                    Toast.makeText(this, "Invalid number format!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please enter a valid number!", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void saveServiceInterval(float minutes) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(SERVICE_INTERVAL, minutes);
        editor.apply();
    }

  private static OkHttpClient getUnsafeOkHttpClient() {
    try {
      // Create a trust manager that does not check certificates
      final TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustManager() {
          @Override
          public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {}

          @Override
          public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {}

          @Override
          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[]{};
          }
        }
      };

      // Set SSL context to skip certificate checking
      final SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
      final javax.net.ssl.SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      // Create OkHttpClient with SSL bypass configuration
      OkHttpClient.Builder builder = new OkHttpClient.Builder();
      builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
      builder.hostnameVerifier((hostname, session) -> true);

      return builder.build();
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
  }

    public static void callAPI(Context context) {
      OkHttpClient okHttpClient = getUnsafeOkHttpClient();
      AndroidNetworking.initialize(context, okHttpClient);

      AndroidNetworking.get(url)
        .setPriority(Priority.LOW)
        .build()
        .getAsJSONArray(new JSONArrayRequestListener() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<Data> dataArrayList = new ArrayList<>();
                    Set<String> setUUIDs = new HashSet<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject O = response.getJSONObject(i);
                        String message = O.getString("message");
                        String number = O.getString("number");
                        String uuid = O.getString("uuid");
                        Data data = new Data(message, number, uuid);
                        if (!setUUIDs.contains(uuid)) {
                            dataArrayList.add(data);
                            dbHandler.addNewCourse(uuid, message, number);
                            setUUIDs.add(uuid);
                        }
                    }
                    sendAllMess(dataArrayList);
                } catch (JSONException e) {
                    Log.e(TAG, "Error parsing response", e);
                }
            }

            @Override
            public void onError(ANError anError) {
                Log.e(TAG, "API Call Error: " + anError.toString());
            }
        });
    }

    private static void callAPIPost(String uuid, String mess, String phone) {
        AndroidNetworking.post(url)
        .addBodyParameter("uuid", uuid)
        .setPriority(Priority.LOW)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                dbHandler.addNewCourse(uuid, mess, phone);
            }

            @Override
            public void onError(ANError anError) {
                Log.e(TAG, "Error sending message: " + anError.toString());
            }
        });
    }

    private static void sendAllMess(ArrayList<Data> dataArrayList) {
        ArrayList<Data> dataArray = dbHandler.getAllProducts();
        // Todo save data log in database.
        for (int i = 0; i < dataArrayList.size(); i++) {
            Data data = dataArrayList.get(i);
            sendMessage(data.getNumber(), data.getMessage());
            callAPIPost(data.getUuid(), data.getMessage(), data.getNumber());
            String currentDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
            String logMessage = currentDateTime + ": " + data.getNumber() + "\n";
            LogHelper.appendLog(logMessage);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_SMS) {
          if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
              sendSMS();
          } else {
              Toast.makeText(this, "SMS permission denied", Toast.LENGTH_SHORT).show();
          }
        } else if (requestCode == PERMISSION_REQUEST_INTERNET) {
          if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
              showPermissionDeniedDialog();
          }
        }
    }

    private void showPermissionDeniedDialog() {
        new AlertDialog.Builder(this)
              .setTitle("Permission Required")
              .setMessage("Internet permission is required to use this feature. Please enable it in the app settings.")
              .setPositiveButton("Go to Settings", (dialog, which) -> {
                  Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                  Uri uri = Uri.fromParts("package", getPackageName(), null);
                  intent.setData(uri);
                  startActivity(intent);
              })
              .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
              .show();

    }

    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(URL, url);
        editor.apply();
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();
    }

    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        url = sharedPreferences.getString(URL, "");
    }

    public void updateViewsUrl() {
        txtURL.setText(url);
    }

}
