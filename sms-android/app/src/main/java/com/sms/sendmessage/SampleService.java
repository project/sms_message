package com.sms.sendmessage;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SampleService extends Service {

    private static final int NOTIF_ID = 1;
    public static final String NOTIF_CHANNEL_ID = "Channel_Id";
    private static final String CHANNEL_NAME = "Foreground Service Channel";
    private IBinder mBinder;
    private boolean mAllowRebind;

    private ScheduledExecutorService scheduledExecutorService;

    private PowerManager.WakeLock wakeLock;

    private void startForegroundService() {
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIF_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Service is running in the background")
                .setContentIntent(pendingIntent);

        startForeground(NOTIF_ID, builder.build());
    }

    @Override
    public void onCreate() {
      super.onCreate();
      createNotificationChannel();

      // Launch foreground service
      startForegroundService();

      // Create WakeLock with FLAG_KEEP_SCREEN_ON to keep device always on.
      PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
      if (powerManager != null) {
      wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SampleService:WakeLock");
      wakeLock.acquire();
      }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
      SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREFS, MODE_PRIVATE);
      float intervalMinutes = sharedPreferences.getFloat(MainActivity.SERVICE_INTERVAL, 1); // Default to 1 minutes.

      if (intent != null && intent.hasExtra("intervalMinutes")) {
          intervalMinutes = intent.getFloatExtra("intervalMinutes", intervalMinutes);
      }
      // Run Scheduled.
      startScheduledTask(intervalMinutes);
      return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return mBinder; }

    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    @Override
    public void onRebind(Intent intent) {}

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop schedule.
        if (scheduledExecutorService != null && !scheduledExecutorService.isShutdown()) {
            scheduledExecutorService.shutdownNow();
        }
        // Release WakeLock Service.
        if (wakeLock != null && wakeLock.isHeld()) {
          wakeLock.release();
        }
    }

    private void createNotificationChannel() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        NotificationChannel serviceChannel = new NotificationChannel(
          NOTIF_CHANNEL_ID,
          CHANNEL_NAME,
          NotificationManager.IMPORTANCE_DEFAULT
        );
        NotificationManager manager = getSystemService(NotificationManager.class);
        if (manager != null) {
          manager.createNotificationChannel(serviceChannel);
        }
      }
    }

  public void startScheduledTask(float intervalMinutes) {
    int intervalSeconds = (int) (intervalMinutes * 60);
    scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        try {
          MainActivity.callAPI(SampleService.this);
        } catch (Exception e) {
          e.printStackTrace();
          // Handle the exception as needed
        }
      }
    }, 0, intervalSeconds, TimeUnit.SECONDS);
  }
}
