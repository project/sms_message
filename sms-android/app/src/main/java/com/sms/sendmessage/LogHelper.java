package com.sms.sendmessage;

import android.widget.TextView;

import java.lang.ref.WeakReference;

public class LogHelper {
  private static WeakReference<TextView> logViewRef;

  public static void setLogView(TextView logView) {
    logViewRef = new WeakReference<>(logView);
  }

  public static void appendLog(String message) {
    TextView logView = logViewRef.get();
    if (logView != null) {
      logView.post(() -> logView.append(message));
    }
  }
}
