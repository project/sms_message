<?php

namespace Drupal\sms_message\Plugin\Action;

use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\Action\Plugin\Action\UnpublishAction;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Delete sms message action.
 */
#[Action(
  id: 'sms_message_sent_action',
  label: new TranslatableMarkup('Mark sent SMS'),
  type: 'sms_message'
)]
class SmsMarkSentAction extends UnpublishAction {
}
