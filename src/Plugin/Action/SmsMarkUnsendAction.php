<?php

namespace Drupal\sms_message\Plugin\Action;

use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\Action\Plugin\Action\PublishAction;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Delete sms message action.
 */
#[Action(
  id: 'sms_message_unsend_action',
  label: new TranslatableMarkup('Mark unsend SMS'),
  type: 'sms_message'
)]
class SmsMarkUnsendAction extends PublishAction {
}
