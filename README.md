Drupal sms message module
========

his module is do it with your Android phone. It is able to send bulk SMS
with Drupal.

Once you have installed this module, you will have an interface to manage
sent SMS messages. Module will provide a token. You need install the app send
sms [apk - 6M](https://drive.google.com/file/d/1qfyBYJAy-JRLNAZMq4YwazM7NAnUCoSd/view)
on your android phone.

After install you need to test by send sms it will ask permission for send sms
you must accept it. After that you add endpoint sms from drupal module, and
click on button On Service. This app request the module's API every 2 minutes
(You can set timer 0.5min = 30s). If any SMS from drupal has not been sent,
it will be downloaded and sent by your SMS phone.

How it works
========

This module will create entity sms content on drupal. You can create SMS
via /admin/content/sms-message or entityTypeManager programmatically
`
$sms = \Drupal::entityTypeManager()->getStorage('sms_message')->create([
  'type' => 'sms_message',
  'number' => $phoneNumber,
  'message' => ['value' => $message],
  'uid' => $user_id,
]);
$sms->save();
`
Goto module configuration, you'll get url endpoint for add to your android phone

You can build your app with Android Studio, source code in sms-android folder,
you will get native android app, After installing the app, you can configure
the endpoint drupal url from configuration note it, Launch android app and click
on the Enable service button. This application will check the unsent sms list on
the server drupal & send it to the client via your sms on Android phone. You can
add '?action=delete' query to the endpoint, meaning it download the SMS message
once and mark it as sent, which causes the SMS message to be sent only once
(even if the SMS message was not actually sent)

To send bulk sms to your customer base. You need create customer type with phone
field as required. You'll need a taxonomy that contains the sms template.
You just create a VBO views with customer type & then enable VBO sms message.
Select phone field in config and vocabulary term. when sending bulk message,
It'll ask to provide sms template you can select your sms term from taxonomy
